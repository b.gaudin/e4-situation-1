<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}
include_once "$racine/modele/bd.resto.inc.php";

// creation du menu burger
$menuBurger = array();
$menuBurger[] = Array("url" => "./?action=recherche&critere=nom", "label" => "Recherche par nom");
$menuBurger[] = Array("url" => "./?action=recherche&critere=adresse", "label" => "Recherche par adresse");

// critere de recherche par defaut
$critere = "nom";
if (isset($_GET["critere"])) {
    $critere = $_GET["critere"];
}
// recuperation des donnees GET, POST, et SESSION
// recherche par nom      -- au départ les zones de saisies sont non remplies
$nomR="";
if (isset($_POST["nomR"]))
{
$nomR = $_POST["nomR"];
}
$voieAdrR="";
$villeR="";
$cpR="";
// recherche par adresse
if (isset($_POST["villeR"]) && isset($_POST["cpR"]) && isset($_POST["voieAdrR"]))
{
$villeR = $_POST["villeR"];
$cpR = $_POST["cpR"];
$voieAdrR = $_POST["voieAdrR"];
}
$listeRestos=array();

// appel des fonctions permettant de recuperer les donnees utiles a l'affichage 
// Si on provient du formulaire de recherche : $critere indique le type de recherche à effectuer
if (!empty($_POST)) {
    switch ($critere) {
        case 'nom':
            // recherche par nom
            if ($nomR!=""){
            $listeRestos=getRestosByNomR($nomR);}
            //echo "test : ".$nomR;
            break;
        case 'adresse':
            // recherche par adresse
            $listeRestos=getRestosByAdresse($voieAdrR, $cpR, $villeR);

            break;
    }
}

// traitement si necessaire des donnees recuperees
;

// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "Recherche d'un restaurant";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueRechercheResto.php";
if (!empty($_POST)){
include "$racine/vue/vueResultRecherche.php";}
include "$racine/vue/pied.html.php";
?>
