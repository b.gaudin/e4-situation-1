<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $racine="..";
}
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION
;

// appel des fonctions permettant de recuperer les donnees utiles a l'affichage 
$listeRestos = getRestos();

// traitement si necessaire des donnees recuperees
$tout = array();
$lesPhotos = array();
for ($i = 0; $i < count($listeRestos); $i++)
{
 $lesPhotos = getPhotosByIdR($listeRestos[$i]['id']);
 $tout[$i] = $lesPhotos[0]['chemin']; 
}

// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "Liste des restaurants répertoriés";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueListeRestos.php";
include "$racine/vue/pied.html.php";
?>