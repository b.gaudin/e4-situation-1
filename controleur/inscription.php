<?php

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    $racine = "..";
}
include_once "$racine/modele/bd.utilisateur.inc.php";

// creation du menu burger
$menuBurger = array();
$menuBurger[] = Array("url"=>"./?action=connexion","label"=>"Connexion");
$menuBurger[] = Array("url"=>"./?action=inscription","label"=>"Inscription");


$inscrit = false;  // servira à savoir si l'inscription est validée (ok) ou non
$msg=""; 
$insc="";
// utilisé dans les vues notamment pour indiquer si l'inscription n'a pas pu être enregistrée
// recuperation des donnees GET, POST, et SESSION
if (isset($_POST["mailU"]) && isset($_POST["mdpU"]) && isset($_POST["pseudoU"])) {   //si toutes les valeurs ont été saisies

    if ($_POST["mailU"] != "" && $_POST["mdpU"] != "" && $_POST["pseudoU"] != "") {   // et qu'elles ne sont pas vides
        $mailU = $_POST["mailU"];
        $mdpU = $_POST["mdpU"];
        $pseudoU = $_POST["pseudoU"];

        // enregistrement des donnees en bdd -  A compléter
        $insc="OK";
        addUtilisateur($mailU, $mdpU, $pseudoU);
        
     }
else {
    $insc="";
    $msg="Renseigner tous les champs...";    
    }
}

    // appel du script de vue qui permet de gerer l'affichage des donnees en fonction du retour d'enregistrement en bdd (/!\ l'insertion peut ne pas avoir lieu, dans ce cas il faut revenir à la vue d'inscription





$titre = "Inscription";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueInscription.php";
include "$racine/vue/pied.html.php";

?>
